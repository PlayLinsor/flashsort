﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using FleshSort;



namespace FleshSort
{
    public partial class Form2 : Form
    {
        public string[,] fileex = new string[20, 30];
        public string[,] real;
        int id = 0;
        public Form2(string[,] fileexx)
        {
            InitializeComponent();
            real = fileexx;
            real[0, 0] = "0";
            for (int f1 = 0; f1 < 20; f1++)
            {
                for (int f2 = 0; f2 < 30; f2++)
               {
                    if (fileexx[f1, f2] != null) fileex[f1, f2] = fileexx[f1, f2];
                }
           }
            
                
        }
        private void GetList()
        {
            listBox1.Items.Clear();
            listBox2.Items.Clear();
            int idx1 = 0, idx2 = 0;
            do
            {
                idx1++;
                try
                {
                    listBox1.Items.Add(fileex[idx1, 0]);
                }
                catch { }
            }
            while (idx1 < 20 - 1);
            do
            {
                idx2++;
                try { if (fileex[1, idx2]!=null) listBox2.Items.Add(fileex[1, idx2]); }
                catch { }
            } while (idx2 < 30 - 1);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        }

        private void Form2_Shown(object sender, EventArgs e)
        {
            GetList();
        }

        private void Form2_Load(object sender, EventArgs e)
        {

        }

        private void listBox1_Click(object sender, EventArgs e)
        {
            listBox2.Items.Clear();
            int idx2 = 0,it=Convert.ToInt16(listBox1.SelectedIndex+1);
            do
            {
                idx2++;
                try { listBox2.Items.Add(fileex[it, idx2]); }
                catch { }
            } while (idx2 < 30 - 1);


        }

        private void listBox1_DoubleClick(object sender, EventArgs e)
        {
            textBox1.Text = listBox1.SelectedItem.ToString();
            label1.Text = "Введите название папки";
            listBox1.Enabled = false;
            listBox2.Enabled = false;
            groupBox1.Visible = true;
            id = 2;
            
        }

        private void button4_Click(object sender, EventArgs e)
        {
            groupBox1.Visible = false;
            listBox1.Enabled = true;
            listBox2.Enabled = true;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (id == 2)
            {
                fileex[Convert.ToInt16(listBox1.SelectedIndex + 1), 0] = textBox1.Text;
                GetList();
                groupBox1.Visible = false;
                listBox1.Enabled = true;
                listBox2.Enabled = true;
            }
            if (id == 3)
            {
                int lo = 1;
                lo = Convert.ToInt16(listBox1.SelectedIndex + 1);
                if (lo == 0) lo = 1;
                fileex[lo,Convert.ToInt16(listBox2.SelectedIndex + 1)] = textBox1.Text;
                GetList();
                groupBox1.Visible = false;
                listBox1.Enabled = true;
                listBox2.Enabled = true;
            }
            if (id == 4)
            {
                int lo = listBox1.Items.Count+1;
                fileex[lo, 0] = textBox1.Text;
                GetList();
                groupBox1.Visible = false;
                listBox1.Enabled = true;
                listBox2.Enabled = true;
            }
            if (id == 5)
            {
                fileex[Convert.ToInt16(listBox1.SelectedIndex + 1), listBox2.Items.Count + 1] = textBox1.Text;
                GetList();
                groupBox1.Visible = false;
                listBox1.Enabled = true;
                listBox2.Enabled = true;
            
            }
            id = 0;
        }

        private void listBox2_DoubleClick(object sender, EventArgs e)
        {
            textBox1.Text = listBox2.SelectedItem.ToString();
            label1.Text = "Введите название расширения";
            listBox1.Enabled = false;
            listBox2.Enabled = false;
            groupBox1.Visible = true;
            id = 3;
        }

        private void добавитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            textBox1.Text = "_Новая категория";
            label1.Text = "Введите название папки";
            listBox1.Enabled = false;
            listBox2.Enabled = false;
            groupBox1.Visible = true;
            id = 4;
        }

        private void удалитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            textBox1.Text = ".txt";
            label1.Text = "Введите название расширения";
            listBox1.Enabled = false;
            listBox2.Enabled = false;
            groupBox1.Visible = true;
            id = 5;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            real[0, 0] = "0";
            Close();
            
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
           
            for (int f1 = 0; f1 < 20; f1++)
            {
                for (int f2 = 0; f2 < 30; f2++)
                {
                    if (fileex[f1, f2] != null) real[f1, f2] = fileex[f1, f2];
                  //  if (fileex[f1, f2] == "") real[f1, f2] = null; 
                }
            }
            real[0, 0] = "1";
            Close();

        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK == MessageBox.Show(null, "Деиствительно хотите удалить расширение ?", "Вопрос", MessageBoxButtons.OKCancel, MessageBoxIcon.Question))
            {
               int lo = 1;
                lo = Convert.ToInt16(listBox1.SelectedIndex + 1);
                if (lo == 0) lo = 1;
                fileex[lo, Convert.ToInt16(listBox2.SelectedIndex + 1)] = "-";
                GetList();
            }
        }
    }
}
