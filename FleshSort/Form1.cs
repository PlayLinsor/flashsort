﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace FleshSort
{
    
   
    public partial class Form1 : Form
    {
        int line = 0;
        public int MaxDir = 20;
        public int MaxExt = 30;
        public string[,] fileex = new string[20, 30];
        string cuDir = "";
        string TemDir = "";
        bool autosort, rezerv;
        string FormName;
        public Form1()
        {
            InitializeComponent();
            FormName = this.Text;
            Application.ApplicationExit += new EventHandler(this.OnApplicationExit);
            FileStream fs = new FileStream("settings.ini", FileMode.Open);
            StreamReader sr = new StreamReader(fs);
            
            int ida = 0, idb = 0;

            try
            {

                string s = sr.ReadLine();
                string[] ss = s.Split('|');
                if (ss[0].Trim().ToLower() == "true") autosort = true;
                else autosort = false;
                if (ss[1].Trim().ToLower() == "true") rezerv = true;
                else rezerv = false;
                if (autosort == false) AutoToolStripMenuItem.Checked = false;

                s = sr.ReadLine();
                while (s != null)
                {
                    ss = s.Split('|');
                    ida++;
                    if (ss[1]!=null) fileex[ida, 0] = ss[1]; // Имя папки
                    string[] s2 = ss[0].Split(' ');
                    idb = 0;
                    while (idb != s2.Length - 1)
                    {
                        idb++;
                        if (s2[idb - 1]!= null) fileex[ida, idb] = s2[idb - 1];
                    }

                    s = sr.ReadLine();
                }
                //fileex[0, 0] = ida;
            }
            catch { MessageBox.Show("Проблема с файлом настроек"); }
            finally
            {
                sr.Close();
                fs.Close();
            }
        } // Загрузка настроек

        // Копируем 1 файл
        private void CopyFileF(string item,string Curdir)
        {
            string CopyDir = "_Все прочее";
            int idx1 = 0, idx2 = 0;
            // Есть ли расширение в настройках, в какую папку кидать
            string ex = Path.GetExtension(item);
            do
            {
                idx1++;
                idx2 = 0;
                do
                {
                    idx2++;
                    if (fileex[idx1, idx2] == ex) CopyDir = fileex[idx1, 0];
                } while (idx2 < MaxExt - 1);
            }
            while (idx1 < MaxDir - 1);

            string curr = Curdir+"\\"+CopyDir; // Конечная директория
            if (!File.Exists(curr)) Directory.CreateDirectory(curr); // Если нет то создаем
            string Copyl = curr + "\\" + Path.GetFileName(item).Trim();
            int Temp_indx = 0;
            while (File.Exists(Copyl)) // Если есть 2 одинаковых файла
            {
                Temp_indx++;
                Copyl = curr + "\\" + Temp_indx + "_" + Path.GetFileName(item).Trim();
            }
            try                // Копируем
            {
                Text = FormName + " " + Path.GetFileName(item);
                File.Copy(item, Copyl);
                Application.DoEvents();
            }
            catch {
               
                toolStripStatusLabel1.Text = "Ошибка нахождения пути"; 
            }
            try // снимаем галочку только для чтения и удаляем его
            {
                if ((File.GetAttributes(item) & FileAttributes.ReadOnly)== FileAttributes.ReadOnly)
                {
                    File.SetAttributes(item, FileAttributes.Normal);
                }
                File.Delete(item);
            }
            catch {
               
                toolStripStatusLabel1.Text = "Ошибка удаления файла"; 
            }
        }
        // Считываем директории
        private void getAllFiles(string dir, UInt16 fieldSize)
        {
            DirectoryInfo dirInfo = new DirectoryInfo(dir);
            FileInfo[] files = dirInfo.GetFiles();
            foreach (FileInfo file in files)
            {
                 string dr = dirInfo.ToString();
                 int idt1 = 0;
                  while (dr.IndexOf('\\')!= -1)
                 {
                      idt1=dr.IndexOf('\\');
                      dr = dr.Remove(0, idt1+1);
                     // idt2 = idt2 + idt1;
                 }
                bool sl=false;
                for (int f = 0; f <= MaxDir-1; f++)
                    if (dr.ToString() == fileex[f, 0]) sl = false; 
                if (sl == false) CopyFileF(dirInfo + "\\" + file.Name,cuDir);
                // 
            }
            DirectoryInfo[] dirs = dirInfo.GetDirectories();
            foreach (DirectoryInfo d in dirs)
            {
                getAllFiles(d.FullName, (UInt16)(fieldSize + 4));
                try
                {
                    Directory.Delete(d.FullName);
                }
                catch {
                    MessageBox.Show(null, "Незможно завершить перемещение \n Невозможно удалить исходный файл", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Application.Exit();
                }
            }
        }
        // Общий старт//////////////
        private void copyToDir()
        {
            listBox1.AllowDrop = false;
            toolStripProgressBar1.ProgressBar.Maximum = listBox1.Items.Count;
            toolStripProgressBar1.Value = 0;
            toolStripProgressBar1.ProgressBar.Visible = true;
            toolStripStatusLabel1.Visible = false;
            Application.DoEvents();
            // основная функция
            string item;
            for (int f = 0; f <= listBox1.Items.Count - 1; f++)
            {
                item = listBox1.Items[f].ToString();
                if (File.Exists(item)) // копирование файлов без директорий 
                {
                    CopyFileF(item, Path.GetDirectoryName(item));
                }

                if (Directory.Exists(item))
                {
                    TemDir = DateTime.Now.Millisecond.ToString();
                    cuDir = Path.GetDirectoryName(item) + "\\" + TemDir;
                    Directory.CreateDirectory(cuDir);
                    getAllFiles(item, 0);
                    Directory.Delete(item);
                    Directory.Move(cuDir, item);
                }
                toolStripProgressBar1.Value = f;
                Application.DoEvents();
            }  // законценно все копирование
            Text = FormName;
            toolStripStatusLabel1.Text = "Завершено копирование";
            toolStripStatusLabel1.Visible = true;
            Application.DoEvents();
            listBox1.Items.Clear();
            toolStripProgressBar1.ProgressBar.Visible = false;
            listBox1.AllowDrop = true;
            listBox1.Tag = 0;
        }
        // Перетягивание на форму файла или директрории
        private void listBox1_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop, false) == true)
            {
                e.Effect = DragDropEffects.All;
            }
        }
        // Перетягивание на форму файла или директрории
        private void listBox1_DragDrop(object sender, DragEventArgs e)
        {
            if (listBox1.Tag=="0") listBox1.Items.Clear();
            listBox1.Tag = "1";
            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);

            foreach (string file in files)
            {
                line++;
                listBox1.Items.Add(file);
            }
            if (line != 0)
            {
                if (autosort == true) copyToDir();
                    else toolStripStatusLabel1.Text = "Ожидание подверждения"; 
            }
        }
        // Редактор файла с настройками 
        private void сортировкаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form2 fr = new Form2(fileex);

            fr.ShowDialog();

            if (fileex[0, 0] == "1")
            {
                FileStream fs = new FileStream("settings.ini", FileMode.Create);
                StreamWriter sr = new StreamWriter(fs);
                try
                {
                    string writeline = autosort.ToString() + "|" + rezerv.ToString();
                    sr.WriteLine(writeline);
                    for (int f1 = 1; f1 < 20; f1++)
                    {
                        writeline = "";
                        for (int f2 = 1; f2 < 30; f2++)
                        {
                            writeline = writeline + fileex[f1, f2] + " ";
                        }
                        writeline = writeline + "|" + fileex[f1, 0];
                        sr.WriteLine(writeline);
                    }

                }
                catch { MessageBox.Show("Проблема с файлом настроек"); }
                sr.Close();
                fs.Close();
            }
        }

        private void AutoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (autosort == true)
            {
                AutoToolStripMenuItem.Checked = false;
                autosort = false;
            }
            else
            {
                AutoToolStripMenuItem.Checked = true;
                autosort = true;
            }
        }

        private void пускToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (listBox1.Tag == "1") copyToDir();
            else
            {
                toolStripProgressBar1.ProgressBar.Visible = false;
                toolStripStatusLabel1.Visible = true;
                toolStripStatusLabel1.Text = "Нет файлов для копирования";
            }
        }
      
        private void удалитьПутьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int i= listBox1.SelectedIndex;
            if (i != 0)
            {
                listBox1.Items.RemoveAt(i);
            }
        }

        private void OnApplicationExit(object sender, EventArgs e)
        {
            FileStream fs = new FileStream("settings.ini", FileMode.Create);
            StreamWriter sr = new StreamWriter(fs);
            try
            {
                string writeline = autosort.ToString() + "|" + rezerv.ToString();
                sr.WriteLine(writeline);
                for (int f1 = 1; f1 < 20; f1++)
                {
                    writeline = "";
                    for (int f2 = 1; f2 < 30; f2++)
                    {
                        writeline = writeline + fileex[f1, f2] + " ";
                    }
                    writeline = writeline + "|" + fileex[f1, 0];
                    sr.WriteLine(writeline);
                }

            }
            catch { MessageBox.Show("Проблема с файлом настроек"); }
            sr.Close();
            fs.Close();

        }

        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
